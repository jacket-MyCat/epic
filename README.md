Epic
=====

### Overview ###
Epic is a core piece of research machinery that centrally apportions disease incidence and/or prevalence into sequela using three main tasks: severity splits, exclusivity adjustments, and the super-squeeze. Epic reads in draw files from DisMod or custom pipelines (saved via save_results_epi) and performs a series of calculations on particular modelable entity ids, in a particular order. The modelable entity ids and order of operations are communicated to Central Computation by the Research Teams.

Sanitized code for the 2019 round of the Global Burden of Disease (GBD) Study can be found here:

https://github.com/ihmeuw/ihme-modeling/tree/master/gbd_2019/shared_code/central_comp/nonfatal/epic

Many developers contributed to this codebase. My contributions were to convert the repository to a package structure, replace luigi workflow management with networkx and jobmon, add a downsampling draw transform, retrieve draws via model version id, and update function calls to handle new decomposition API.
